import csv
import requests
from bs4 import BeautifulSoup

URL = 'https://megachas.ru/catalog/man/'
HEADERS = {
    'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) '
                  'Chrome/97.0.4692.99 Safari/537.36 '
}


def get_content():
    response = requests.get(url=URL, headers=HEADERS)
    soup = BeautifulSoup(response.text, 'lxml')
    pages = int(soup.find('div', class_='pagination').find_all('a')[-1].text)
    # собираем страницы
    for page in range(1, pages + 1):
        list_url = f'https://megachas.ru/catalog/man/?PAGEN_2={page}'

        response = requests.get(url=list_url, headers=HEADERS)
        soup = BeautifulSoup(response.text, 'lxml')

        block_content = soup.find_all('div', class_='n-product')
        list_items = []
        print(f'Парсим страницу номер {page}')
        # достаем данные из конкретного блока
        for item in block_content:
            title = item.find('span', class_='n-product-title').text.replace('\n', '').replace('\r', '').split(
                '\t')  # получаем название и модель
            href = item.find('div', class_='n-product__content').find('a').get('href')
            list_items.append({
                'title': title[4],
                'model': title[-5],
                'price': item.find('span', class_='n-product__price').get_text().strip().replace('i', 'руб.'),
                'href': f'https://megachas.ru{href}'
            })

        # записываем кажду страницу в отдельный .csv файл
        with open(f'file_{page}.csv', 'w', encoding='utf-8', newline='') as file:  # перевод строки ньюлайн
            writer = csv.writer(file, delimiter=',')
            writer.writerow(['Название', 'Модель', 'Цена', 'Ссылка'])
            for item in list_items:
                writer.writerow([item['title'], item['model'], item['price'], item['href']])


def main():
    get_content()


if __name__ == '__main__':
    main()
